Plugins
=======

.. toctree::
   :maxdepth: 1

   adaptativeauthenticationlevel
   autosignin
   bruteforceprotection
   cda
   checkstate
   checkuser
   viewer
   contextswitching
   decryptvalue
   loginhistory
   forcereauthn
   globallogout
   grantsession
   impersonation
   finduser
   notifications
   status
   public_pages
   refreshsessionapi
   resetpassword
   resetcertificate
   restservices
   soapservices
   stayconnected
